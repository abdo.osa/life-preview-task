import React, { Component } from 'react';
import './Preview.css';
import { Carousel } from 'react-bootstrap';


class Preview extends Component {
    state = {
      looooo : {
        width:'300px'
      }
    }
    render() {
      let logo1 = null ,
      logo2 = null ,
      CTA = null;

      // change logo to square
      if(this.props.logoStylesquare && this.props.logoUrl)
      { 
        let logoSquareStyle = {
          width:'100px',
          height:'100px'
        }

        logo1 = (
            <div className="logoStyle">
             <img style={logoSquareStyle} src={this.props.logoUrl}/>
          </div>
        );
       
      }
      // add rounded to logo 
      else if(this.props.logoStyleRounded && this.props.logoUrl)
      {
        let logoRoundedStyle = {
          'borderRadius':'50%'
        }
       
        logo1 = (
             <div className="logoStyle">
             <img style={logoRoundedStyle} src={this.props.logoUrl}/>
          </div>
        ); 
      }

      else
      // change logo width
        if(this.props.widthLogo1 === "250")
        {
          let logowidth = {
            width:'250px'
          }

          logo2 = (
              <div className="logoStyle">
               <img style={logowidth}  src={this.props.logoUrl}/>
            </div>
          );
        }
        else if(this.props.widthLogo1 === "300")
        {
          let logowidth = {
            width:'300px'
          }

          logo2 = (
            <div className="logoStyle">
               <img style={logowidth}  src={this.props.logoUrl}/>
            </div>
          );
        }
        else
        {
          logo2 = (
            <div className="logoStyle">
               <img  src={this.props.logoUrl}/>
            </div>
          );
        }

        
      


     let CTAval ;
        // change CTA Button Text
      if(this.props.CTAtext)
      {
         CTAval = this.props.CTAtext ;

        CTA = (
          <div className="CTAcontainerstyle">
             <a href={this.props.CTAtxtURL}>
             <input style={Object.assign({}, this.props.CTAbacColor, this.props.CTAtxtcolor)}  className="CTAbtn" type="button" value={CTAval} /> 
             </a>
          </div>
        );

      }
      else
      {
         CTAval = "CTA Button";

        CTA = (
          <div className="CTAcontainerstyle">
             <a href={this.props.CTAtxtURL}>
             <input style={Object.assign({}, this.props.CTAbacColor, this.props.CTAtxtcolor)} className="CTAbtn" type="button" value={CTAval} /> 
             </a>
          </div>
        );
      }
      

      return (
        <div className="Preview">
          {/* call logo jsx code  as square or rounded*/}
              {logo1}
              {/* call logo jsx code with new width  */}
              {logo2}

              {/* slider */}
              <Carousel>              
                {
                  this.props.sliderImgs.map((x , index) => {
                    return <Carousel.Item key={index}>
                      <img
                      className="d-block w-100"
                      src={x}
                      alt="First slide"
                      key={index}
                    />
                  </Carousel.Item>
                  })
                }
              </Carousel>

                {/* page title */}
              <p 
              style={Object.assign({}, this.props.TitleBACcolor , this.props.titleColor)} 
              className="titlePage">{this.props.pageTitle}
              </p>

              {CTA}

        </div>
      );
    }
  
  }
  
  
  
  export default Preview;