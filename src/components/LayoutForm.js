import React, { Component } from 'react';
import './LayoutForm.css';
import Preview from './Preview';

class LayoutForm extends Component {
    state = {
      logo:null ,
      sliderImg:[],
      title:null,
      logosquare:false,
      logoRounded:false,
      logoWidth1 : null ,
      titBacCol:null ,
      titColor:null ,
      CTAval:null ,
      CTAbtnBacCol:null ,
      CTAtxtColor:null ,
      CTAURL:null
    }


      //  add title  function
    uploadTitle = (event) => {
      this.setState({
        title:event.target.value
      })
 
    }

      //  change title background color  function
    titleBacgroundColor = (event) => {
      let titleBacColor = event.target.value;

      if(titleBacColor === "red")
      {
        
      this.setState({
        titBacCol:{
          backgroundColor : '#F61050'
        }
      });
      }
      else  if(titleBacColor === "blue")
      {
        this.setState({
          titBacCol:{
            backgroundColor : '#007bff'
          }
        });
      }
      else  if(titleBacColor === "green")
      {
        this.setState({
          titBacCol:{
            backgroundColor : '#0def41'
          }
        });
      }

    }


      //  change title color  function
    titleColor = (event) => {
      let titleBacColor = event.target.value;

      if(titleBacColor === "yellow")
      {
        
      this.setState({
        titColor:{
          color : '#ffbf00'
        }
      });
      }
      else  if(titleBacColor === "blue")
      {
        this.setState({
          titColor:{
            color : '#00dcff'
          }
        });
      }
      else  if(titleBacColor === "orange")
      {
        this.setState({
          titColor:{
            color : '#ff7800'
          }
        });
      }

    }




      //  upload logo function
    uploadLogo = (event) => {

      let images = event.target.files ||  event.dataTransfer.files;

      if (images) {
        let reader = new FileReader();
        reader.onload = (e) => {
            this.setState(
                {logo: e.target.result
                });
        };
        if(images[0])
        {
          reader.readAsDataURL(images[0]);
        }       
    }
    }

      //  change logo to square function
    styleLogoSquare = () => {
      const logoStyle = this.state.logosquare;
      this.setState({logosquare:!logoStyle})  
    }


//  make rounded to logo function
    styleLogorounded = () => {
      const logoStyle = this.state.logoRounded;
      this.setState({logoRounded:!logoStyle})
      
    }

      //  change logo width function
    styleLogoWidth = (event) => {
      
      let logoWidth = event.target.value;
     
      if(logoWidth === "250")
      {
      this.setState({logoWidth1:logoWidth});
      }
      else  if(logoWidth === "300")
      {
        this.setState({logoWidth1:logoWidth})
      }
      else  if(logoWidth === "200")
      {
        this.setState({logoWidth1:logoWidth})
      }

    }


    //  upload slider images function
    uploadSliderImg = (event) => {
      let images = event.target.files ||  event.dataTransfer.files;

      if (images) {
        let reader = new FileReader();
        reader.onload = (e) => {
            
          this.setState(state => {
            const sliderImg = state.sliderImg.concat(e.target.result);
      
            return {
              sliderImg
            };
          });
        };
        reader.readAsDataURL(images[0]);
    }
    

    }

    //  add text to CTA Button function
    AddCTAbuttonText = (event) => {
      this.setState({
        CTAval:event.target.value
      })    
    }


    //  change CTA button background color function
    CTAbtnBacgroundColor = (event) => {
      let CTAbtnBacColor = event.target.value;

      if(CTAbtnBacColor === "red")
      {
        
      this.setState({
        CTAbtnBacCol:{
          backgroundColor : '#F61050'
        }
      });

      }
      else  if(CTAbtnBacColor === "blue")
      {
        this.setState({
          CTAbtnBacCol:{
            backgroundColor : '#007bff'
          }
        });
      }
      else  if(CTAbtnBacColor === "green")
      {
        this.setState({
          CTAbtnBacCol:{
            backgroundColor : '#0def41'
          }
        });
      }

    }


    //  change CTA button text color function
    CTAtxtColor = (event) => {
      let CTAtxtColor = event.target.value;

      if(CTAtxtColor === "yellow")
      {
        
      this.setState({
        CTAtxtColor:{
          color : '#ffbf00'
        }
      });
      }
      else  if(CTAtxtColor === "blue")
      {
        this.setState({
          CTAtxtColor:{
            color : '#00dcff'
          }
        });
      }
      else  if(CTAtxtColor === "orange")
      {
        this.setState({
          CTAtxtColor:{
            color : '#ff7800'
          }
        });
      }

    }


    //  add CTA button URL function
    CTAbtnURL = (event) => {
      this.setState({
        CTAURL:event.target.value
      })
    }



    render() {
      return (
       
           <div className="LayoutForm">
              <img className="logoImg" src="https://res.cloudinary.com/appgain/image/upload/v1534373384/appgain/logo.png" />
            
              <div className="form">
                {/* add title*/}
                <label className="labeStyle">Title</label>
                <input className="inputs" type="text" onChange={(event) => {this.uploadTitle(event)}} />

                <br/>
                {/* change title background color*/}
                <label className="labeStyle">Title background color</label>
                <select  className="inputs" onChange={(event) => {this.titleBacgroundColor(event)}} >
                  <option></option>
                  <option value="red">red</option>
                  <option value="blue">blue</option>
                  <option value="green">green</option>
                </select>

                 <br/>
                {/* change title color*/}
              <label className="labeStyle">Title color</label>
              <select  className="inputs" onChange={(event) => {this.titleColor(event)}} >
                <option></option>
                <option value="yellow">yellow</option>
                <option value="blue">blue</option>
                <option value="orange">orange</option>
              </select>

                 <br/>
                 {/* upload logo image*/}
                <label className="labeStyle">Logo</label>
                <input  className="inputs" type="file" onChange={(event) => {this.uploadLogo(event)}} />
                <br/>

                {/* square button */}
                <input className="buttons"  type="button" value="Toggle Logo square" onClick={this.styleLogoSquare}/>
                <br/>
                {/* change logo width*/}
                <label className="labeStyle">Logo Width</label>
                <select  className="inputs" onChange={(event) => {this.styleLogoWidth(event)}} >
                  <option></option>
                  <option value="200">200</option>
                  <option value="250">250</option>
                  <option value="300">300</option>
                </select>

               <br/>

                {/* rounded button */}
                <input className="buttons" type="button" value="Toggle Logo rounded" onClick={this.styleLogorounded}/>

                <br/>
                <label className="labeStyle">Add slider images</label>
                <input  className="inputs" type="file" onChange={(event) => {this.uploadSliderImg(event)}} />
               <br/>
                {/* CTA Button change text*/}
                <label className="labeStyle">Enter CTA Button Text</label>
                <input  className="inputs" type="text" onChange={(event) => {this.AddCTAbuttonText(event)}} maxLength="10"/>

                <br/>
                {/* CTA Button change background color*/}
                <label className="labeStyle">CTA Button background color</label>
                <select  className="inputs" onChange={(event) => {this.CTAbtnBacgroundColor(event)}} >
                  <option></option>
                  <option value="red">red</option>
                  <option value="blue">blue</option>
                  <option value="green">green</option>
                </select>

              <br/>
              {/* CTA Button change text color*/}
              <label className="labeStyle">CTA Button Text color</label>
              <select  className="inputs" onChange={(event) => {this.CTAtxtColor(event)}} >
                <option></option>
                <option value="yellow">yellow</option>
                <option value="blue">blue</option>
                <option value="orange">orange</option>
              </select>


              <br/>
              {/* CTA Button URL*/}
              <label className="labeStyle">Enter CTA Button URL</label>
              <input  className="inputs" type="text" onChange={(event) => {this.CTAbtnURL(event)}} />

              </div>

              {/* preview component */}
              <Preview 
                logoStylesquare={this.state.logosquare} 
                logoStyleRounded={this.state.logoRounded} 
                logoUrl={this.state.logo} 
                sliderImgs={this.state.sliderImg}
                pageTitle={this.state.title}
                widthLogo1={this.state.logoWidth1}
                TitleBACcolor={this.state.titBacCol}
                titleColor={this.state.titColor}
                CTAtext={this.state.CTAval}
                CTAbacColor={this.state.CTAbtnBacCol}
                CTAtxtcolor={this.state.CTAtxtColor}
                CTAtxtURL={this.state.CTAURL}
              />
              
        </div>
       
      );
    }
  
  }
  
  
  
  export default LayoutForm;
  
